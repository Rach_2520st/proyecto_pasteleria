-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Danydan
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Danydan
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Danydan` ;
USE `Danydan` ;

-- -----------------------------------------------------
-- Table `Danydan`.`Perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Perfil` (
  `id_perfil` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  UNIQUE INDEX `id_perfil_UNIQUE` (`id_perfil` ASC) VISIBLE,
  PRIMARY KEY (`id_perfil`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Usuario` (
  `rut` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `apellido` VARCHAR(45) NULL,
  `telefono` INT NULL,
  `contraseña` VARCHAR(45) NULL,
  `correo` VARCHAR(45) NULL,
  `Perfil_id_perfil` INT NOT NULL,
  PRIMARY KEY (`rut`, `Perfil_id_perfil`),
  UNIQUE INDEX `rut_UNIQUE` (`rut` ASC) VISIBLE,
  INDEX `fk_Usuario_Perfil_idx` (`Perfil_id_perfil` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_Perfil`
    FOREIGN KEY (`Perfil_id_perfil`)
    REFERENCES `Danydan`.`Perfil` (`id_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`Compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Compra` (
  `id_compra` INT NOT NULL,
  `fecha_compra` DATETIME NULL,
  `estado` TINYINT(1) NULL,
  `Usuario_rut` INT NOT NULL,
  `Usuario_Perfil_id_perfil` INT NOT NULL,
  PRIMARY KEY (`id_compra`),
  UNIQUE INDEX `id_compra_UNIQUE` (`id_compra` ASC) VISIBLE,
  INDEX `fk_Compra_Usuario1_idx` (`Usuario_rut` ASC, `Usuario_Perfil_id_perfil` ASC) VISIBLE,
  CONSTRAINT `fk_Compra_Usuario1`
    FOREIGN KEY (`Usuario_rut` , `Usuario_Perfil_id_perfil`)
    REFERENCES `Danydan`.`Usuario` (`rut` , `Perfil_id_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`Tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Tipo` (
  `id_tipo` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id_tipo`),
  UNIQUE INDEX `id_tipo_UNIQUE` (`id_tipo` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`Producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Producto` (
  `id_producto` INT NOT NULL,
  `descripcion` VARCHAR(150) NULL,
  `fecha_ingr` DATE NULL,
  `nombre` VARCHAR(45) NULL,
  `precio` INT NULL,
  `nombre_foto` VARCHAR(1000) NULL,
  `Tipo_id_tipo` INT NOT NULL,
  PRIMARY KEY (`id_producto`, `Tipo_id_tipo`),
  UNIQUE INDEX `id_producto_UNIQUE` (`id_producto` ASC) VISIBLE,
  INDEX `fk_Producto_Tipo1_idx` (`Tipo_id_tipo` ASC) VISIBLE,
  CONSTRAINT `fk_Producto_Tipo1`
    FOREIGN KEY (`Tipo_id_tipo`)
    REFERENCES `Danydan`.`Tipo` (`id_tipo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`Detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Detalle` (
  `Compra_id_compra` INT NOT NULL,
  `Producto_id_producto` INT NOT NULL,
  `cantidad` INT NULL,
  `valor` VARCHAR(45) NULL,
  PRIMARY KEY (`Compra_id_compra`, `Producto_id_producto`),
  INDEX `fk_Compra_has_Producto_Producto1_idx` (`Producto_id_producto` ASC) VISIBLE,
  INDEX `fk_Compra_has_Producto_Compra1_idx` (`Compra_id_compra` ASC) VISIBLE,
  CONSTRAINT `fk_Compra_has_Producto_Compra1`
    FOREIGN KEY (`Compra_id_compra`)
    REFERENCES `Danydan`.`Compra` (`id_compra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_has_Producto_Producto1`
    FOREIGN KEY (`Producto_id_producto`)
    REFERENCES `Danydan`.`Producto` (`id_producto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`Sucursal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`Sucursal` (
  `id_sucursal` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `direccion` VARCHAR(150) NULL,
  PRIMARY KEY (`id_sucursal`),
  UNIQUE INDEX `id_sucursal_UNIQUE` (`id_sucursal` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Danydan`.`inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Danydan`.`inventario` (
  `Sucursal_id_sucursal` INT NOT NULL,
  `Producto_id_producto` INT NOT NULL,
  `stock` INT NULL,
  PRIMARY KEY (`Sucursal_id_sucursal`, `Producto_id_producto`),
  INDEX `fk_Sucursal_has_Producto_Producto1_idx` (`Producto_id_producto` ASC) VISIBLE,
  INDEX `fk_Sucursal_has_Producto_Sucursal1_idx` (`Sucursal_id_sucursal` ASC) VISIBLE,
  CONSTRAINT `fk_Sucursal_has_Producto_Sucursal1`
    FOREIGN KEY (`Sucursal_id_sucursal`)
    REFERENCES `Danydan`.`Sucursal` (`id_sucursal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Sucursal_has_Producto_Producto1`
    FOREIGN KEY (`Producto_id_producto`)
    REFERENCES `Danydan`.`Producto` (`id_producto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
