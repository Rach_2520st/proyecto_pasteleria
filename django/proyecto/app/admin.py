from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UsuarioCreationForm, UsuarioChangeForm
from .models import Perfil, Usuario, Compra, Tipo, Producto, Detalle, Sucursal, Inventario

#
class UsuarioAdmin(UserAdmin):
    add_form = UsuarioCreationForm
    form = UsuarioChangeForm
    model = Usuario
    list_display = ["email", "username", "first_name", "last_name"]

#
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion', 'fecha_ingr')
    list_filter = ('precio', 'fecha_ingr')
    search_fields = ('nombre',)

#Por el momento no se me ocurre que otra cosa puede hacer el admin
#class IdiomaPublicacionAdmin(admin.ModelAdmin):
    #list_display = ('publicacion', 'idioma', 'fecha')
    #list_filter = ('idioma', 'fecha')

#
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Producto, ProductoAdmin)
#admin.site.register(IdiomaPublicacion, IdiomaPublicacionAdmin)

#
#admin.site.register(Publicacion)
#admin.site.register(Revista)
#admin.site.register(Idioma)
#admin.site.register(IdiomaPublicacion)
