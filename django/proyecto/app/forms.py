from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Perfil, Usuario, Compra, Tipo, Producto, Detalle, Sucursal, Inventario

###
class PerfilForm(forms.ModelForm):
	class Meta:
		model = Perfil
		fields = ("id", "nombre")
		
		widgets = {
			"nombre": forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True})
			}
			 
class UsuarioCreationForm(UserCreationForm):
  # recibe el id_perfil para agregarla al formulario.
	def __init__(self, *args, **kwargs):
		Perfil = kwargs.pop('Perfil')
		super(UsuarioCreationForm, self).__init__(*args, **kwargs)
		self.fields['Perfil'].initial = Perfil
    
    class Meta:
		model = Usuario
		fields = ("Perfil", "rut", "username", "email", "telefono", "contrasena")
		widgets = {
			'Perfil': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
			'rut': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
			'username': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			'email': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			'telefono': forms.NumberInput(attrs={'type': 'integer', 'class': 'form-control', 'required': True}),
			#revisar si esta correcto el uso de la contraseña
			'contrasena': forms.TextInput(attrs={'type': 'password', 'class': 'form-control', 'required': True}),
			
        }

class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

###
class CompraForm(forms.ModelForm):
	#recibe el id_perfil como foreign key
	def __init__(self, *args, **kwargs):
        Perfil = kwargs.pop('Perfil')
        super(CompraForm, self).__init__(*args, **kwargs)
        self.fields['Perfil'].initial = Perfil
        
    #recibe el rut de usuario como foreign key
	def __init__(self, *args, **kwargs):
        Usuario = kwargs.pop('Usuario')
        super(CompraForm, self).__init__(*args, **kwargs)
        self.fields['Usuario'].initial = Usuario
    
    class Meta:
        model = Compra
        # Se agrega el id de compra?
        fields = ("id_perfil", "rut","fecha_compra", "estado")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'id_perfil': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True 'required': True}),
            'rut': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True, 'required': True, 'required': True}),
            'fecha_compra': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
            
            #revisar si es que boolean se agrega como formulario o de que forma agregarlo
           #'estado': forms.booleanInput(attrs={'type': 'boolean', 'class': 'form-control', 'required': True}),
        }
        
###
class TipoForm(forms.ModelForm):
        
    class Meta:
        model = Tipo
        fields = ("nombre")
        
        widgets = {
            'nombre': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
        }
        
        

class ProductoForm(forms.ModelForm):
	#recibe el id_tipo como foreign key
	def __init__(self, *args, **kwargs):
        Tipo = kwargs.pop('Tipo')
        super(ProductoForm, self).__init__(*args, **kwargs)
        self.fields['Tipo'].initial = Tipo
	class Meta:
		model = Producto
		#se agrega el id de producto?
		fields = ("id_tipo", "nombre", "descripcion", "fecha_ingr", "precio", "nombre_foto")
		
		widgets = {
			'id_tipo': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True 'required': True}),
			'nombre': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			'descripcion': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			'fecha_ingr': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
			'precio': forms.NumberInput(attrs={'type': 'number', 'class': 'form-control', 'required': True}),
			'nombre_foto': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			}
     


class DetalleForm(forms.ModelForm):
	#recibe el id_compra como foreign key
	def __init__(self, *args, **kwargs):
        Tipo = kwargs.pop('Compra')
        super(DetalleForm, self).__init__(*args, **kwargs)
        self.fields['Compra'].initial = Compra
	#recibe el id_producto como foreign key
	def __init__(self, *args, **kwargs):
        Tipo = kwargs.pop('Producto')
        super(DetalleForm, self).__init__(*args, **kwargs)
        self.fields['Producto'].initial = Producto
	
	class Meta:
		model = Detalle
		
		fields = ("id_compra", "id_producto", "cantidad", "valor")
		
		widgets = {
			'id_compra': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True 'required': True}),
			'id_producto': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True 'required': True}),
			'cantidad': forms.NumberInput(attrs={'type': 'number', 'class': 'form-control', 'required': True}),
			'valor': forms.NumberInput(attrs={'type': 'number', 'class': 'form-control', 'required': True}),
			
			}
	
	
class SucursalForm(forms.ModelForm):
	class Meta:
		model = Sucursal
		
		fields =("nombre", "direccion")
		
		widgets = {
			'nombre': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			'direccion': forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'required': True}),
			
			}
			
class InventarioForm(forms.ModelForm):
		#recibe el id_sucursal como foreign key
	def __init__(self, *args, **kwargs):
        Tipo = kwargs.pop('Sucursal')
        super(InventarioForm, self).__init__(*args, **kwargs)
        self.fields['Sucursal'].initial = Sucursal
	#recibe el id_producto como foreign key
	def __init__(self, *args, **kwargs):
        Tipo = kwargs.pop('Producto')
        super(InventarioForm, self).__init__(*args, **kwargs)
        self.fields['Producto'].initial = Producto
	
	class Meta:
		model = Inventario
		fields = ("id_sucursal", "id_producto", "stock")
		
		widgets = {
			'id_sucursal': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True 'required': True}),
			'id_producto': forms.NumberInput(attrs={'class': 'form-control', 'readonly': True 'required': True}),
			'stock': forms.NumberInput(attrs={'type': 'number', 'class': 'form-control', 'required': True}),
			
			}
			
		
