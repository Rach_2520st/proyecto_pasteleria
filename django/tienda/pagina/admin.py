from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UsuarioCreationForm, UsuarioChangeForm
from .models import Compra, Detalle, Inventario, Sucursal, Usuario, Tipo, Producto

#
class UsuarioAdmin(UserAdmin):
    add_form = UsuarioCreationForm
    form = UsuarioChangeForm
    model = Usuario
    list_display = ["email", "username", "first_name", "last_name","telefono","rut"]

#
class ProductoAdmin(admin.ModelAdmin):
    list_display = ("nombre","descripcion","fecha_ingr","precio","nombre_foto","id_tipo")
    list_filter = ('fecha_ingr','id_tipo')
    search_fields = ('nombre',)

#
class CompraAdmin(admin.ModelAdmin):
    list_display = ("fecha_compra","estado","usuario")
    list_filter = ('fecha_compra','estado')
    search_fields = ('usuario',)

#
class SucursalAdmin(admin.ModelAdmin):
    list_display = ("nombre","direccion")
    search_fields = ('nombre',)

#
class DetalleAdmin(admin.ModelAdmin):
    list_display = ("producto","valor","cantidad","id_compra")
    list_filter = ('valor','producto')
    search_fields = ('valor',)

#
class InventarioAdmin(admin.ModelAdmin):
    list_display = ("id_producto","id_sucursal","stock")
    list_filter = ('stock','id_producto')
    search_fields = ('stock',)

#
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Compra, CompraAdmin)
admin.site.register(Sucursal, SucursalAdmin)
admin.site.register(Detalle, DetalleAdmin)
admin.site.register(Inventario, InventarioAdmin)

#
admin.site.register(Tipo)

