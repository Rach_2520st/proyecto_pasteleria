from django.contrib.auth.models import AbstractUser
from django.db import models

	
# https://learndjango.com/tutorials/django-custom-user-model
# modelo User personalizado.
class Usuario(AbstractUser):
    # atributos adicionales.
	rut =  models.CharField(primary_key=True, unique=True, max_length=100)
	email = models.EmailField('email address', unique = True)
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['rut','username', 'first_name', 'last_name']
	telefono = models.IntegerField(null=True)

	def __str__(self):
		return self.first_name + ' ' + self.last_name

#################################################################################

class Compra(models.Model):
    
	fecha_compra = models.DateField()
	estado = models.BooleanField()
	usuario = models.ForeignKey(to=Usuario, on_delete=models.CASCADE, null=True)


class Tipo(models.Model):
	nombre = models.CharField(max_length=100)
    
	def __str__(self):
		return self.nombre
         

class Producto(models.Model):
    nombre = models.CharField(max_length=250)
    descripcion = models.CharField(max_length=500)
    fecha_ingr = models.DateField()
    precio = models.IntegerField()
    nombre_foto = models.CharField(max_length=500)
    id_tipo = models.ForeignKey(to=Tipo, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
         return self.nombre


class Detalle(models.Model):
	cantidad = models.IntegerField()
	valor = models.IntegerField()
	id_compra = models.ForeignKey(to=Compra, on_delete=models.CASCADE, null=True)
	producto = models.ForeignKey(to=Producto, on_delete=models.CASCADE, null=True)


class Sucursal(models.Model):
	nombre = models.CharField(max_length=45)
	direccion = models.CharField(max_length=50)
	
	def __str__(self):
		return self.nombre + ' ' + self.direccion

	
class Inventario(models.Model):
	stock = models.IntegerField()
	id_sucursal = models.ForeignKey(to=Sucursal, on_delete=models.CASCADE, null=True)
	id_producto = models.ForeignKey(to=Producto, on_delete=models.CASCADE, null=True)
        
