# Generated by Django 4.0.4 on 2022-06-13 01:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pagina', '0007_alter_inventario_unique_together'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='detalle',
            unique_together=set(),
        ),
        migrations.AlterUniqueTogether(
            name='inventario',
            unique_together=set(),
        ),
    ]
