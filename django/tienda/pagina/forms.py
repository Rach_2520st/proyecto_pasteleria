from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Detalle, Inventario, Usuario, Producto

###
class UsuarioCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

###
class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ("nombre","descripcion","fecha_ingr","precio","nombre_foto","id_tipo")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'descripcion': forms.TextInput(attrs={'type': 'form-control', 'class': 'form-control', 'required': True}),
            'fecha_ingr': forms.DateInput(attrs={'class': 'date', 'required': True}),
            'precio': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
            'nombre_foto': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'id_tipo': forms.Select(attrs={'class': 'form-control', 'required': True}),
        }

###
class DetalleForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        producto = kwargs.pop('producto')
        super(DetalleForm, self).__init__(*args, **kwargs)
        self.fields['producto'].initial = producto

    def __init__(self, *args, **kwargs):
        valor = kwargs.pop('producto')
        super(DetalleForm, self).__init__(*args, **kwargs)
        self.fields['precio'].initial = valor

    class Meta:
        model = Detalle
        fields = ("cantidad","valor","id_compra","producto")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'cantidad': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
            'valor': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
            'id_compra': forms.SelectMultiple(attrs={'class': 'form-control', 'required': True}),
            'producto': forms.SelectMultiple(attrs={'class': 'form-control', 'required': True}),
        }

###
class InventarioForm(forms.ModelForm):
    class Meta:
        model = Inventario
        fields = ("stock","id_sucursal","id_producto")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'stock': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
            'id_sucursal': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'id_producto': forms.SelectMultiple(attrs={'class': 'form-control', 'required': True}),
        }