from django.conf import settings

def home(request):
	return render(request, "principal.html", {'settings': settings})
