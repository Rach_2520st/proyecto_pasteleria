from django.conf.urls import url
from principal import views


urlpatterns = [
	path('admin/', admin.site.urls),
	url(r'^$', views.home, name='home'),
	url(r'^perfiles/$', views.perfiles, name='perfiles'),
]
	
	
